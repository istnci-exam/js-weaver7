// JS파일 형식

//Console
console.log(5);
console.log(10);

//Comments
// Opening line
console.log("It was love at first sight.");

console.log("The first time Yossarian saw the chaplain he fell madly in love with him.");
console.log("Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.");
console.log("The doctors were puzzled by the fact that it wasn't quite jaundice.");
console.log("If it became jaundice they could treat it.");
console.log("If it didn't become jaundice and went away they could discharge him.");
console.log("But this just being short of jaundice all the time confused them.");

//Data Types
console.log("JavaScript");
console.log(2011);
console.log("Woohoo! I love to code! #codecademy");
console.log(20.49);

//Arithmetic Operators
console.log(34 + 3.5);
console.log(2021 - 1969);
console.log(65 / 240);
console.log(0.2708 * 100);
console.log(0.2708 * 100);
